# montecarlo
Calculate the value of PI with Monte Carlo metod in parallel (Second Internship Project in 2012)

## Description
In this study, Pi values were calculated as distributed over created on a cluster using the IPC( Inter-process communication ) technology.

<img src="https://gitlab.com/kyellow/montecarlo/raw/internship/screenshot/mcpi.png" width="400" height="400"/>

## * [MonteCarlo Project Old Codes Made During Intership (Branch -> internship)] (https://gitlab.com/kyellow/montecarlo/blob/internship/README.md)

## * [Then MonteCarlo Project Was Recoded With MPICH2 (Branch -> mpich)] (https://gitlab.com/kyellow/montecarlo/blob/mpich/README.md)
